import math
import random
import heapq
import json
import copy
import os
import pprint
import logging

from collections import defaultdict

from .scoring_utils import VARIANT_CONFIG_PATH, TTypes

from .penalty_schema2 import Schema2
from .penalty_schema4 import Schema4


class PenaltyCalculator:
    """
    Note that the worlds use a 2D coordinate system where x increases in the right direction
    and y increases in the downward direction, similar to that of the coordinate system used by HTML Canvas

    parameters:
        log (int): Integer flag for setting level of logging
                   (0 = None, 1 = logging.info, 2 = logging.error, 3 = logging.debug)
    returns:
    """
    def __init__(self, tiles, AOI, start, hiker, end, wind=7, log=0):
        # Set up logging
        self.logging_level = log
        if log == 0:
            self.logging_level = logging.CRITICAL
        elif log == 1:
            self.logging_level = logging.INFO
        elif log == 2:
            self.logging_level = logging.ERROR
        elif log == 3:
            self.logging_level = logging.DEBUG

        self.logger = logging.Logger('MAIN')
        self.logger.setLevel(self.logging_level)
        
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        handler = logging.StreamHandler()
        handler.setLevel(self.logging_level)
        handler.setFormatter(formatter)
        self.logger.addHandler(handler)
        
        # Trees and low hill at 1
        self.elevation_obstacles_1 = [TTypes.FOREST, TTypes.LOW_HILL]
        # Towers and hill at 2
        self.elevation_obstacles_2 = [TTypes.AIRTRAFFIC_TOWER, TTypes.FIREWATCH_TOWER, TTypes.HILL]
        # Foothill at 3
        self.elevation_obstacles_3 = [TTypes.FOOTHILL, ]
        # Mountain at 4
        self.elevation_obstacles_4 = [TTypes.MOUNTAIN, ]

        self.start = tuple(start)
        self.end = tuple(end)
        self.AOI = tuple(AOI)
        self.hiker = tuple(hiker)
        self.wind_dir = wind

        self.tile_map = tiles
        self.logger.debug("Tile map is: {}".format(tiles))
        self.elevation_map = [[0 for _ in range(len(tiles[0]))] for _ in range(len(tiles))]

        # Build elevation map
        for y, row in enumerate(self.tile_map):
            for x, col in enumerate(row):
                if self.tile_map[y][x] in self.elevation_obstacles_1:
                    self.elevation_map[y][x] = 1
                elif self.tile_map[y][x] in self.elevation_obstacles_2:
                    self.elevation_map[y][x] = 2
                elif self.tile_map[y][x] in self.elevation_obstacles_3:
                    self.elevation_map[y][x] = 3
                elif self.tile_map[y][x] in self.elevation_obstacles_4:
                    self.elevation_map[y][x] = 4

        self.logger.debug("Elevation map is: {}".format(self.elevation_map))

        self.schema = {
            2: Schema2(self),
            4: Schema4(self),
            5: Schema4(self, variant='Enhanced_Flier'),
            6: Schema4(self, variant='Enhanced_Flier_Commander'),
            7: Schema4(self, variant='Enhanced_Receiver'),
            8: Schema4(self, variant='Limited_Mobility_Receiver'),
            9: Schema4(self, variant='Extreme_Flier'),
            10: Schema4(self, variant='Extreme_Flier_Commander'),
            11: Schema4(self, variant='Reciever_Convenience'),
            12: Schema4(self, variant='Extreme_Package_Safety'),
        }

    def reinit(self, tiles, AOI, start, hiker, end, wind=7):
        self.start = start
        self.end = end
        self.AOI = AOI
        self.hiker = hiker
        self.wind_dir = wind

        self.tile_map = tiles
        self.elevation_map = [[0 for _ in range(len(tiles[0]))] for _ in range(len(tiles))]

        # Build elevation map
        for y, row in enumerate(self.tile_map):
            for x, col in enumerate(row):
                if self.tile_map[y][x] in self.elevation_obstacles_1:
                    self.elevation_map[y][x] = 1
                elif self.tile_map[y][x] in self.elevation_obstacles_2:
                    self.elevation_map[y][x] = 2
                elif self.tile_map[y][x] in self.elevation_obstacles_3:
                    self.elevation_map[y][x] = 3
                elif self.tile_map[y][x] in self.elevation_obstacles_4:
                    self.elevation_map[y][x] = 4

    def load_variants(self, schema):
        """
        Score variations - multiplicative tweaks too small to warrant an entirely new schema.
        Loads variant weight configurations into scoring engine and returns dictionary with weights per factor.
        Default weight is 1.0

        parameters:
            schema (string): identifier for schema (IE "base", "schema4", etc)
        returns:
            weight_dict (dict): dictionary with weights.
        """
        variants = {}

        # Load config file and populate variants dictionary
        with open(VARIANT_CONFIG_PATH, 'r') as config:
            config_dict = json.load(config)

            # Populate variants
            for variant in config_dict[schema]:
                variant_weights = defaultdict(lambda: 1.0)
                for factor, weight in variant["weights"].items():
                    variant_weights[factor] = weight

                variants[variant["name"]] = variant_weights

            # Add on custom variant
            variants['CUSTOM'] = defaultdict(lambda: 1.0)
        
        return variants

    def set_custom_schema(self, values, base=4):
        if base == 2:
            self.schema['CUSTOM'] = Schema2(self, variant='CUSTOM')
        elif base == 4:
            self.schema['CUSTOM'] = Schema4(self, variant='CUSTOM')
        else:
            self.schema['CUSTOM'] = Schema4(self, variant='CUSTOM')

        new_values = copy.deepcopy(values)

        self.schema['CUSTOM'].variants['CUSTOM'].update(new_values)
        self.schema['CUSTOM'].init_factors()

        self.logger.debug("Setting custom schema weights to: {}".format(self.schema['CUSTOM'].variants['CUSTOM']))

    def calculate_penalty(self, craft_path, drop, schema=2, print_penalty=False, partial_flight=False):
        """
        parameters:
            craft_path (list): ordered list of length 4 tuples (x, y, z, heading) in coordinates local to AOI
            drop (tuple): planned drop point with (x, y, z, heading).  Must be one of the segments in craft_path
            schema (int): integer enum representing which schema to use
            print_penalty (boolean): flag for whether or not to print the final penalty
            partial_flight (boolean): flag for whether or not the flight has completed
        returns:
            penalties (dictionary): dictionary of different factor penalties
        """

        penalties = {}
        schema_key = schema

        if schema != 'CUSTOM' and (type(schema) is str or type(schema) is float):
            schema_key = int(schema)

        try:
            s = self.schema[schema_key]
            s.set_schema_num(schema_key)
        except KeyError as e:
            logging.error("{} | schema {} is unavailable, using schema 4 instead.".format(e, schema_key))
            s = self.schema[4]
            s.set_schema_num(4)

        penalties = s.calculate(craft_path, drop, partial_flight)

        if print_penalty:
            self.logger.setLevel(logging.INFO)
            pp = pprint.PrettyPrinter(indent=3)
            p_string = pp.pformat(penalties)
            self.logger.info(p_string)
            self.logger.setLevel(self.logging_level)

        return penalties

    def get_factors(self, schema):
        return self.schema[schema].get_factors()

    def get_single_penalty_narrative(self, penalties):
        narrative = ''

        s = self.schema[penalties['schema']]
        narrative = s.get_narrative(penalties)

        return penalties, narrative

    def get_comparative_narrative(self, penalty1, penalty2, commander1, commander2, constraints):

        s = self.schema[4]
        narrative = s.get_comp_narrative(penalty1, penalty2, commander1, commander2, constraints)

        return narrative

    # Local coordinates inside AOI
    def inside_AOI(self, x, y):
        return 0 <= x and x < self.AOI[2] and 0 <= y and y < self.AOI[3]
