# flight_scoring

Python 3.6+ functions for scoring a COGLE Flight Trace.  These functions only consider events and actions that occur within a defined Area Of Interest (AOI).  As of 7/9/2020, flight-scoring uses native python, so no further packages are required.

# Installation

Make sure that your desired virtual environment is enabled, if you would like to use one.

Do:  
    `pip3 install git+https://gitlab.com/cogle-public/flight_scoring`


# Usage

See `tests/test_scoring.py` for example usage.

    from flight_scoring.flight_scoring import PenaltyCalculator

    # aoi: size 4 tuple with (x, y, width, height)
    # tile_map: size (x, y) 2D list of integers that represents the map
    # start, end: size 4 tuples with (x, y, z, heading), represents craft positions
    # hiker: size 4 tuple with (x, y, z, heading).  Heading is required but not impactful to path planning for scoring
    # craft_path: list of size 4 tuples (x, y, z, heading) that represents states of a craft in flight
    # drop: size 4 tuple with (x, y, z, heading).  Represents drop point of craft, MUST BE ONE OF THE ELEMENTS IN CRAFT_PATH
    # schema: integer flag that denotes which scoring scheme to use.  Defaults to schema 2.

    pc = PenaltyCalculator(tile_map, aoi, start, hiker, end)
    penalties = pc.calculate_penalty(craft_path, drop, schema=2)

## Relevant Spaces

A penalty breakdown in the form of a multi-dimensional list is included with the actual penalties, called "Relevant Spaces" or as it is labelled within the returned dictionary, "spaces".  These spaces are positions within the area of interest that contribute towards a particular factor, and are separated into categories that reflect each factor.  For any factor, the standardized format for a breakdown would be: 

    [[timestep, score_at_timestep, [[score_at_space, (x, y)], ...]], ...]

The first dimension of the list structure stores penalty by time
* `timestep` is the time with respect to the start of the trace at which the penalty is accrued.
* `score_at_timestep` is the cumulative score for that `timestep` in the trace.

The second dimension stores penalty by positions
* `score_at_space` is how much penalty a particular space contributes to the `score_at_timestep`
* `(x, y)` coordinates for the space that contributes `score_at_space`

Each breakdown is inserted into a JSON/Dictionary object, converted into a JSON friendly string, and included under the `spaces` return value.

## Penalty for schema 2

Penalties are applied with the fact that what is being scored is a plan, not an actual flight.

| code | full name | description |  
|---|---|---|  
| csi | craft safety ingress | Penalty for flying too close to obstacles on ingress route. |  
| cdi | craft distance ingress | Penalty accrued for flying on ingress route, longer routes = more penalty. |  
| pi | package inaccessibility | Penalty for dropping the package where it is at risk of landing on an obstacle that makes it difficult for the hiker to access it. |  
| pd | package damage | Penalty for dropping the package where it is at risk of being damaged. |  
| hc | hiker climbing | penalty for dropping the package in a place where the hiker will have to do some amount of climbing to reach it. |  
| hrl | hiker route length | Penalty for dropping the package in a place where the hiker will have to do some amount of climbing to reach it. |  
| hrc | hiker river crossing | Penalty for dropping the package in a place where the hiker will have to cross some body of water to reach it. |  
| hlos | hiker line of sight to package | Penalty for dropping the package in a place where the hiker is unable to see where it could land. |  
| cse | craft safety egress | Penalty for flying too close to obstacles on egress route. |  
| cde | craft distance egress | Penalty accrued for flying on egress route, longer routes = more penalty. |  
| cel | craft leave error | Penalty for leaving the area of interest. |  
| ced | craft destination error | Penalty for ending the flight in a position that differs from the specified end point. |  

## Penalty for schema 4
Penalties are applied with the fact that what is being scored is a plan, not an actual flight.

| code | full name | description |  
|---|---|---|  
| csi | craft safety ingress | Penalty for flying too close to obstacles on ingress route. |  
| cfi | craft flying duration ingress | Number of minutes it takes for flying to the drop point. |  
| pdp | package damage penalty | Penalty for dropping the package where it is at risk of being damaged. |  
| pdur | package delay duration | Delay in minutes added to hiker route due to difficulty of accessing the package at landing point. |  
| pip | package inaccessibility penalty | Penalty incurred for dropping with a possible landing position inaccessible to the hiker. |
| hc | hiker climbing | Number of minutes the hiker takes to climb up or down terrain while hiking to the package. |  
| hrd | hiker route duration | Number of minutes the hiker takes to hike across flat terrain to reach the package (2 dimensions). |  
| hrc | hiker river crossing | Number of minutes the hiker takes to cross some body of water to reach the package. |  
| hlos | hiker line of sight to package | Number of additional minutes taken while hiking to a package due to searching past high elevation obstacles. |  
| cse | craft safety egress | Penalty for flying too close to obstacles on egress route. |  
| cfe | craft flying egress | Number of minutes it takes for flying to the drop point. |   

## Variants
Some schema that are simply scaled versions of other schema.  These variations are listed and configured within the `/flight-scoring/flight_scoring/variant_config.json` file.  As of 7/9/2020, the only variants available are of Schema 4 (schema 5, 6, 7, 8, 9, 10, 11, 12).