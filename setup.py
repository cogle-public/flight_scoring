from setuptools import setup, find_packages
 
setup(name='flight_scoring',
      version='1.5.14',
      url='https://gitlab.com/COGLEProject/flight_scoring',
      author='Jacob Le',
      author_email='jacob.le@parc.com',
      description='Python functions for scoring a COGLE Flight Traces',
      packages=find_packages(exclude=['tests']),
      include_package_data=True,
      package_data={
            "flight_scoring": ["*.json"],
      },
      long_description=open('README.md').read(),
      zip_safe=False)
