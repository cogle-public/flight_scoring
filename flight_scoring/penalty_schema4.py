import math
import random
import heapq
import json
import copy

from .scoring_utils import TTypes, Dirs, euclidean_distance


X = 0
Y = 1
Z = 2
H = 3


class Schema4():
    """
    This scoring schema references COGLE document "2020-05-27 HMF Terminology v9",
    to be reffered to as the "Document"
    """
    def __init__(self, engine, variant='base', schema_num=4):
        self.engine = engine
        self.variant = variant

        # Factors
        self.CRAFT_SAFETY_INGRESS = 'csi'
        self.CRAFT_SAFETY_EGRESS = 'cse'
        self.CRAFT_FLYING_INGRESS = 'cfi'
        self.CRAFT_FLYING_EGRESS = 'cfe'
        self.HIKER_LINE_OF_SIGHT = 'hlos'
        self.HIKER_RIVER_CROSSING = 'hrc'
        self.HIKER_CLIMBING = 'hc'
        self.HIKER_FOREST_DELAY = 'hfd'
        self.HIKER_ROUTE_DURATION = 'hrd'
        self.PACKAGE_DMG_PENALTY = 'pdp'
        self.PACKAGE_INACCESS_PENALTY = 'pip'

        self.found = False  # Check if hiker has reached package
        self.hiker_path = []    # Stores hiker path

        # Movement
        self.straight_moves = [Dirs.NORTH, Dirs.EAST,
                               Dirs.SOUTH, Dirs.WEST]
        # Hiker:
        # Hiker possible moves (relative positions)
        self.hiker_moves = [(0, -1, Dirs.NORTH), (1, -1, Dirs.NORTHEAST),
                            (1, 0, Dirs.EAST), (1, 1, Dirs.SOUTHEAST),
                            (0, 1, Dirs.SOUTH), (-1, 1, Dirs.SOUTHWEST),
                            (-1, 0, Dirs.WEST), (-1, -1, Dirs.NORTHWEST)]

        # Hiker accessable tile types
        self.hiker_access = [TTypes.GRASS, TTypes.FOREST, TTypes.ROAD, TTypes.RIVER, TTypes.LOW_HILL]

        # Line of Sight obstacles
        self.elevation_obs = [TTypes.FOREST, TTypes.FIREWATCH_TOWER, TTypes.AIRTRAFFIC_TOWER,
                              TTypes.LOW_HILL, TTypes.HILL, TTypes.FOOTHILL, TTypes.MOUNTAIN]

        # Buildings
        self.buildings = [TTypes.FIREWATCH_TOWER, TTypes.AIRTRAFFIC_TOWER]

        # Score variations - multiplicative tweaks too small to warrant an entirely new schema
        # Referencing "2020-07-01 Notes on Agents Scenarios Explanations v2", to be referred to as the Config
        # By default, a penalty multiplier is 1.0
        self.variants = self.engine.load_variants("4")
        self.schema_num = schema_num

        self.weights = self.variants[self.variant]

        self.init_factors()

    def init_factors(self):
        self.weights = self.variants[self.variant]

        # Craft flying duration in minutes - weights applied in function
        self.C_DAY_DUR = 1440.0
        self.C_STRAIGHT_DUR = 4.0
        self.C_ANGLED_DUR = 6.0
        self.C_ALT_CHANGE_DUR = 2.0

        # Craft safety penalty - weights applied in function
        self.C_SAFETY_RISK = 1.0
        self.C_CRASH_PENALTY = 100.0

        # Hiker pathfinding
        self.H_MOVE_S = 10.0
        self.H_MOVE_A = 15.0
        self.H_MOVE_ELEVATION = 5.0
        self.H_MOVE_FOREST = 5.0
        self.H_MOVE_RIVER = 10.0

        # hiker hiking duration in minutes
        self.H_STRAIGHT = 10.0 * self.weights[self.HIKER_ROUTE_DURATION]
        self.H_ANGLE = 15.0 * self.weights[self.HIKER_ROUTE_DURATION]
        self.H_FOREST = 5.0 * self.weights[self.HIKER_FOREST_DELAY]
        self.H_RIVER = 10.0 * self.weights[self.HIKER_RIVER_CROSSING]
        self.H_ELEVATION = 5.0 * self.weights[self.HIKER_CLIMBING]
        self.H_LOS_DELAY = 5.0 * self.weights[self.HIKER_LINE_OF_SIGHT]

        # Package penalty + duration
        self.P_DMG = 100.0 * self.weights[self.PACKAGE_DMG_PENALTY]
        self.P_DMG_BUILDING = 30.0 * self.weights[self.PACKAGE_DMG_PENALTY]
        self.P_DMG_TREE = 30.0 * self.weights[self.PACKAGE_DMG_PENALTY]
        self.P_DMG_WATER = 30.0 * self.weights[self.PACKAGE_DMG_PENALTY]
        self.P_OUT_OF_REACH = 100.0 * self.weights[self.PACKAGE_INACCESS_PENALTY]
        self.P_ACCESS_WATER = 30.0 * self.weights[self.PACKAGE_INACCESS_PENALTY]
        self.P_ACCESS_FOREST = 30.0 * self.weights[self.PACKAGE_INACCESS_PENALTY]
        self.P_ALT_2_SPACES = 9.0
        self.P_ALT_3_SPACES = 25.0

    def set_schema_num(self, schema_num):
        if type(schema_num) is int:
            self.schema_num = schema_num

    def get_factors(self):
        return self.variants[self.variant]

    def get_narrative(self, penalties):
        narrative = 'PLACEHOLDER'

        return narrative

    def get_comp_narrative(self, penalty1, penalty2, commander1, commander2, constraints):
        winner_gist = ''
        context_gist = ''
        debias_gist = ''
        winner = 'None'
        benefit = 'None'

        diffs = [(0, "None")]
        drop1 = penalty1['drop_point']
        drop2 = penalty2['drop_point']
        # route1_length = len(penalty1['hiker_path'])
        # route2_length = len(penalty2['hiker_path'])

        self.engine.logger.debug(penalty1)
        self.engine.logger.debug(penalty2)
        self.engine.logger.debug(commander1)
        self.engine.logger.debug(commander2)

        # Flags
        drone1_win = True
        drone2_win = True
        same_drop = drop1 == drop2
        # winner_shorter = False # Does the winner have a shorter hiker path?
        winner_safety_advantage = False # Does the winner have a craft flying skill advantage
        winner_commander_consistent = False # Does the winner have an accurate perception of the hiker (matching the commander’s)?
        winner_knows_fast_hiker = False # Does the winner know differentially that the hiker is fast?
        winner_knows_slow_hiker = False # Does the winner know differentially that the hiker is slow?

        # Variants
        drone1_variant = self.engine.schema[penalty1['schema']].variant
        drone2_variant = self.engine.schema[penalty2['schema']].variant
        cmd1_variant = self.engine.schema[penalty1['schema']].variant
        cmd2_variant = self.engine.schema[penalty2['schema']].variant

        # Schema
        drone1_eval = self.variants[drone1_variant]
        drone2_eval = self.variants[drone2_variant]
        cmd1_eval = self.variants[cmd1_variant]
        cmd2_eval = self.variants[cmd2_variant]

        # Constraint checks
        dur1_fail = commander1['drop_duration'] >= constraints['time']
        dur2_fail = commander2['drop_duration'] >= constraints['time']
        in_safety1_fail = commander1[self.CRAFT_SAFETY_INGRESS] >= constraints['penalty']
        in_safety2_fail = commander2[self.CRAFT_SAFETY_INGRESS] >= constraints['penalty']
        eg_safety1_fail = commander1[self.CRAFT_SAFETY_EGRESS] >= constraints['penalty']
        eg_safety2_fail = commander2[self.CRAFT_SAFETY_EGRESS] >= constraints['penalty']
        inaccess1_fail = commander1[self.PACKAGE_INACCESS_PENALTY] >= constraints['penalty']
        inaccess2_fail = commander2[self.PACKAGE_INACCESS_PENALTY] >= constraints['penalty']
        dmg1_fail = commander1[self.PACKAGE_DMG_PENALTY] >= constraints['penalty']
        dmg2_fail = commander2[self.PACKAGE_DMG_PENALTY] >= constraints['penalty']

        # Generate winner phrase
        if dur1_fail or in_safety1_fail or eg_safety1_fail or inaccess1_fail or dmg1_fail:
            drone1_win = False
        elif drop1 is None:
            drone1_win = False
        if dur2_fail or in_safety2_fail or eg_safety2_fail or inaccess2_fail or dmg2_fail:
            drone2_win = False
        elif drop2 is None:
            drone2_win = False
        
        # Both lose
        if not drone1_win and not drone2_win:
            winner_gist = "Both plans failed because they violated the retrieval constraint or did not drop a package."
        
        # Both win
        elif drone1_win and drone2_win:
            # Check tiebreaker
            if commander1['total_penalty'] == commander2['total_penalty']:
                winner = 'Tie'
                if same_drop:
                    winner_gist = "The drones have the same drop point and tie."
                else:
                    winner_gist = "The drones tie."

            elif commander1['total_penalty'] < commander2['total_penalty']:
                winner = 'Drone 1'
                if same_drop:
                    winner_gist = "The drones have the same drop point, however {}'s plan is better because it accrues the lowest risk penalty".format(winner)
                else:
                    winner_gist = "{}'s plan is better because it accrues the lowest risk penalty and violates no constraints.".format(winner)
            else:
                winner = 'Drone 2'
                if same_drop:
                    winner_gist = "The drones have the same drop point, however {}'s plan is better because it accrues the lowest risk penalty".format(winner)
                else:
                    winner_gist = "{}'s plan is better because it accrues the lowest risk penalty and violates no constraints.".format(winner)

        # Only drone 1 wins
        elif drone1_win:
            winner = 'Drone 1'
            winner_gist = "{}'s plan is the best choice because Drone 2's plan violates the maximum retrieval time.".format(winner)
            benefit = 'retrieval time'

        # Only drone 2 wins
        elif drone2_win:
            winner = 'Drone 2'
            winner_gist = "{}'s plan is the best choice because Drone 1's plan violates the maximum retrieval time.".format(winner)
            benefit = 'retrieval time'

        # Generate context phrase
        if winner == 'Tie':
            context_gist = "The drones drop the package with the same risk"
            if same_drop:
                context_gist = "The drones drop the package with the same risk at the same drop point."
        elif winner != "None":
            # Calculate greatest benefit
            if winner == "Drone 1":
                diffs = [(penalty2[self.CRAFT_SAFETY_INGRESS] - penalty1[self.CRAFT_SAFETY_INGRESS], "ingress safety risk"),
                         (penalty2[self.CRAFT_SAFETY_EGRESS] - penalty1[self.CRAFT_SAFETY_EGRESS], "egress safety risk"),
                         (penalty2[self.PACKAGE_DMG_PENALTY] - penalty1[self.PACKAGE_DMG_PENALTY], "package damage risk"),
                         (penalty2[self.PACKAGE_INACCESS_PENALTY] - penalty1[self.PACKAGE_INACCESS_PENALTY], "package inaccessibility risk")]
            if winner == "Drone 2":
                diffs = [(penalty1[self.CRAFT_SAFETY_INGRESS] - penalty2[self.CRAFT_SAFETY_INGRESS], "ingress safety risk"),
                         (penalty1[self.CRAFT_SAFETY_EGRESS] - penalty2[self.CRAFT_SAFETY_EGRESS], "egress safety risk"),
                         (penalty1[self.PACKAGE_DMG_PENALTY] - penalty2[self.PACKAGE_DMG_PENALTY], "package damage risk"),
                         (penalty1[self.PACKAGE_INACCESS_PENALTY] - penalty2[self.PACKAGE_INACCESS_PENALTY], "package inaccessibility risk")]
                        
            # Sort differences from highest to lowest
            diffs.sort(key=lambda x: x[0], reverse=True)
            benefit = diffs[0][1]

            if not same_drop:
                context_gist = "{} drops the package where there is lower {}, because only it".format(winner, benefit)
            else:
                context_gist = "{} drops the package with lower {} because only it".format(winner, benefit)

            # if winner_route_len > loser_route_len:
            #     winner_shorter = False
            # else:
            #     winner_shorter = True

            # Check for winner craft safety skill advantage
            winner_csr = 0
            loser_csr = 0
            drone1_csr = drone1_eval[self.CRAFT_SAFETY_INGRESS]
            drone2_csr = drone2_eval[self.CRAFT_SAFETY_INGRESS]
            if winner == 'Drone 1':
                winner_csr = drone1_csr
                loser_csr = drone2_csr
            elif winner == 'Drone 2':
                winner_csr = drone2_csr
                loser_csr = drone1_csr
                
            if winner_csr < loser_csr:
                winner_safety_advantage = True
        
            # Check if winner is consistent with commander
            self.engine.logger.debug("COMMANDER 1 eval: {} | COMMANDER 2 eval: {} | DRONE 1 eval: {} | DRONE 2 eval: {}".format(cmd1_eval[self.HIKER_ROUTE_DURATION], cmd2_eval[self.HIKER_ROUTE_DURATION], drone1_eval[self.HIKER_ROUTE_DURATION], drone2_eval[self.HIKER_ROUTE_DURATION]))
            if winner == 'Drone 1' and drone1_eval[self.HIKER_ROUTE_DURATION] == cmd1_eval[self.HIKER_ROUTE_DURATION]:
                winner_commander_consistent = True
            elif winner == 'Drone 2' and drone2_eval[self.HIKER_ROUTE_DURATION] == cmd2_eval[self.HIKER_ROUTE_DURATION]:
                winner_commander_consistent = True

            # Check if winner knows that the hiker is fast or slow
            winner_duration = 0
            loser_duration = 0

            if winner == 'Drone 1':
                winner_duration = cmd1_eval[self.HIKER_ROUTE_DURATION]
                loser_duration = cmd2_eval[self.HIKER_ROUTE_DURATION]
                if winner_duration < loser_duration:
                    winner_knows_fast_hiker = True
                elif winner_duration > loser_duration:
                    winner_knows_slow_hiker = True
            elif winner == 'Drone 2':
                winner_duration = cmd2_eval[self.HIKER_ROUTE_DURATION]
                loser_duration = cmd1_eval[self.HIKER_ROUTE_DURATION]
                if winner_duration < loser_duration:
                    winner_knows_fast_hiker = True
                elif winner_duration > loser_duration:
                    winner_knows_slow_hiker = True

            self.engine.logger.debug("Winner is {}".format(winner))
            self.engine.logger.debug("Winner safety advantage ({} vs {}): {}".format(winner_csr, loser_csr, winner_safety_advantage))
            self.engine.logger.debug("same drop ({} | {}): {}".format(drop1, drop2, same_drop))
            self.engine.logger.debug("winner commander consistent eval: {}".format(winner_commander_consistent))
            self.engine.logger.debug("winner knows fast hiker ({} vs {}): {}".format(winner_duration, loser_duration, winner_knows_fast_hiker))
            self.engine.logger.debug("winner knows slow hiker ({} vs {}): {}".format(winner_duration, loser_duration, winner_knows_slow_hiker))

            if winner_safety_advantage:
                context_gist = "{} can safely fly close to obstacles.".format(context_gist)
            elif not same_drop and winner_commander_consistent and winner_knows_fast_hiker:
                context_gist = "{} knows that the hiker hikes quickly.".format(context_gist)
            elif not same_drop and winner_commander_consistent and winner_knows_slow_hiker:
                context_gist = "{} knows that the hiker hikes slowly.".format(context_gist)

        # TODO get benefit total delta
        total_delta = abs(penalty1['total_penalty'] - penalty2['total_penalty'])

        # Generate debias phrase
        if winner != 'Tie' and winner != 'None' and benefit != 'None':
            if benefit == 'retrieval time':
                debias_gist = "In other missions, the benefits may be lower retrieval time."
            elif diffs[0][0] < (0.4 * total_delta): # 58.5 < 37
                leaf_factor = diffs[0][1]
                if "hiker" in leaf_factor:
                    benefit = "hiker retrieval duration"
                elif "duration" in leaf_factor:
                    benefit = "craft route duration"
                elif "package" in leaf_factor:
                    benefit = "package risk"
                elif "safety" in leaf_factor:
                    benefit = "craft safety risk"
                
            debias_gist = "In other missions, the benefits may be lower {} or lower overall retrieval time.".format(benefit)

        if "violates the maximum retrieval time." in winner_gist:
            context_gist = ""

        return winner_gist, context_gist, debias_gist

    def calculate(self, craft_path, drop, partial_flight):
        """
        Calculate individual factor penalties + delays as outlined in the Document, return in a dictionary
        along with penalty breakdowns for each (spaces).

        parameters:
            craft_path (list): ordered list of size 4 tuples (x, y, z, heading) that represents states of a craft in flight
            drop (tuple): size 4 tuple with (x, y, z, heading).  Represents drop point of craft,
                          MUST BE ONE OF THE ELEMENTS IN CRAFT_PATH.
                          If the craft has not dropped, use None instead.
        returns:
            dictionary with: {
                csi (float):    craft safety ingress
                cfi (float):    craft flying duration ingress
                pdp (float):    package damage penalty
                pip (float):    package inaccessibility penalty
                hc (float):     hiker climbing
                hrd (float):    hiker route duration
                hrc (float):    hiker river crossing
                hfd (float):    hiker forest delay
                hlos (float):   hiker line of sight to package
                cse (float):    craft safety egress
                cfe (float):    craft flying egress
                drop_penalty (float):   drop penalty
                drop_duration (float):  drop duration
                total_penalty (float):  total penalty
                total_duration (float): total duration
                spaces (string):    relevant spaces
            }
            See README.md for more details
        """

        self.engine.logger.debug("Calculating penalty using: {}".format(self.variant))
        self.engine.logger.debug(self.variants[self.variant])

        # Ensure that 'drop' is an acceptable value (tuple or None)
        if (drop is not None) and not (type(drop) is tuple or type(drop) is list):
            raise ValueError("SCHEMA_4: invalid value passed for drop ({})".format(drop))
        elif (drop is not None) and (type(drop) is tuple) and (drop not in craft_path):
            raise ValueError("SCHEMA_4: drop ({}) not in the craft path list".format(drop))

        # Initialize factor values
        craft_flying_ingress = 0.0
        craft_flying_egress = 0.0
        craft_safety_ingress = 0.0
        craft_safety_egress = 0.0
        hiker_river_crossing = 0.0
        hiker_forest_delay = 0.0
        hiker_climbing = 0.0
        hiker_package_los = 0.0
        hiker_route_duration = 0.0
        package_damage = 0.0
        package_inaccessibility = 0.0

        # Relevant spaces - [[timestep, score_at_timestep, [[score_at_space, (x, y)], ...]], ...]
        spaces_flying_ingress = []
        spaces_flying_egress = []
        spaces_safety_ingress = []
        spaces_safety_egress = []
        spaces_river = []
        spaces_forest = []
        spaces_climbing = []
        spaces_los = []
        spaces_route = []
        spaces_damage = []
        spaces_paccess = []

        # Find the drop point and separate into ingress and egress
        before_drop = []
        after_drop = []
        package_land = drop
        drop_index = -1

        for timestep, pos in enumerate(craft_path):
            # Check for equality in drop point X and Y for each position
            if drop and pos[X] == drop[X] and pos[Y] == drop[Y]:
                drop_index = timestep  # Drop index
                break
        
        # Check if drop point was found, split into ingress/egress paths accordingly
        if drop_index != -1:
            # Decision by team that ingres/egress paths will not include drop step, increment by 1
            # Slice the path list to get ingress / egress paths
            before_drop = craft_path[:drop_index+1]
            after_drop = craft_path[drop_index+1:]
        else:   # In case craft does not drop
            before_drop = craft_path

        # Calculate craft flying duration and safety penalty for ingress route
        craft_flying_ingress, spaces_flying_ingress = self.score_craft_flying(before_drop, 0)
        craft_safety_ingress, spaces_safety_ingress = self.score_craft_safety(before_drop, 0)

        # Do not give additional penalties for partial flights. See Document -Not- Dropped Rule 1
        if drop is not None or not partial_flight:
            # planned drop target (where the package would land if it fell straight downwards)
            drop_target = drop
            if drop is not None:
                # use elevation of terrain at drop x and y coordinates as landing altitude
                drop_alt = self.engine.elevation_map[drop[Y]][drop[X]]
                if self.engine.tile_map[drop[Y]][drop[X]] == TTypes.FOREST:
                    drop_alt = 0

                drop_target = (drop[X], drop[Y], drop_alt, drop[H])

            # Hiker route duration
            hiker_difficulty = self.score_hiker_difficulty(self.engine.hiker, drop_target, drop_index)
            hiker_river_crossing, hiker_forest_delay, hiker_climbing, hiker_route_duration, \
                spaces_river, spaces_forest, spaces_climbing, spaces_route = hiker_difficulty # Unpack
            hiker_package_los, spaces_los = self.score_package_los(self.engine.hiker, drop_target, drop_index)

            # Package risk penalty
            package_risk = self.score_package_risk(self.engine.hiker, drop, drop_index)
            package_damage, package_inaccessibility, \
                spaces_damage, spaces_paccess = package_risk  # Unpack

        # Calculate craft flying duration + craft safety penalty for egress route
        craft_flying_egress, spaces_flying_egress = self.score_craft_flying(after_drop, drop_index+1)
        craft_safety_egress, spaces_safety_egress = self.score_craft_safety(after_drop, drop_index+1)

        # Total penalty and duration for ingress + drop.  See Document Scoring Rules 1
        drop_penalty = craft_safety_ingress + package_damage + package_inaccessibility
        drop_duration = craft_flying_ingress + hiker_river_crossing + hiker_climbing + \
            hiker_route_duration + hiker_forest_delay + hiker_package_los

        # Total penalty and duration for overall flight
        total_penalty = craft_safety_egress+drop_penalty
        total_duration = craft_flying_egress+drop_duration

        # Serialize hiker path
        hp = json.dumps(self.hiker_path)

        # Dictionary of spaces relevant to the scoring converted into a JSON friendly string format.
        # Used to pack data for info transfer.
        spaces_string = json.dumps({
            self.CRAFT_SAFETY_INGRESS:  spaces_safety_ingress,
            self.CRAFT_FLYING_INGRESS:  spaces_flying_ingress,
            self.HIKER_RIVER_CROSSING:  spaces_river,
            self.HIKER_FOREST_DELAY:    spaces_forest,
            self.HIKER_CLIMBING:        spaces_climbing,
            self.HIKER_ROUTE_DURATION:  spaces_route,
            self.HIKER_LINE_OF_SIGHT:   spaces_los,
            self.CRAFT_SAFETY_EGRESS:   spaces_safety_egress,
            self.CRAFT_FLYING_EGRESS:   spaces_flying_egress,
            self.PACKAGE_DMG_PENALTY:   spaces_damage,
            self.PACKAGE_INACCESS_PENALTY: spaces_paccess
        })

        # Schema 4
        return {
            self.CRAFT_SAFETY_INGRESS:  craft_safety_ingress,
            self.CRAFT_FLYING_INGRESS:  craft_flying_ingress,
            self.HIKER_RIVER_CROSSING:  hiker_river_crossing,
            self.HIKER_CLIMBING:        hiker_climbing,
            self.HIKER_ROUTE_DURATION:  hiker_route_duration,
            self.HIKER_LINE_OF_SIGHT:   hiker_package_los,
            self.HIKER_FOREST_DELAY:    hiker_forest_delay,
            self.CRAFT_SAFETY_EGRESS:   craft_safety_egress,
            self.CRAFT_FLYING_EGRESS:   craft_flying_egress,
            self.PACKAGE_DMG_PENALTY:   package_damage,
            self.PACKAGE_INACCESS_PENALTY: package_inaccessibility,
            "drop_penalty": drop_penalty,
            "drop_duration": drop_duration,
            "total_penalty": total_penalty,
            "total_duration": total_duration,
            "hiker_path": hp,
            "drop_point": drop,
            "spaces": spaces_string,
            "schema": self.schema_num
        }

    def score_craft_flying(self, craft_path, offset):
        """
        Calculate length of flight in minutes. See Document page 16.
        parameters:
            craft_path (list): list of size 4 tuples (x, y, z, heading) that represents states of a craft in flight
            offset (int): time offset for differentiating between different flight segments (ingress/egress)
        returns:
            duration (float): length of flight in minutes
            flying_relevant_spaces (list): Spaces that caused scores
                                           [[timestep, score_at_timestep, [[score_at_space, (x, y)], ...]], ...]
        """
        duration = 0.0
        crash = False

        # Dual purpose: initialize variables and empty path list check.
        try:
            prev_alt = craft_path[0][Z]
        except IndexError:
            # Return default values if craft_path is empty (did not fly anywhere, no penalty)
            return duration, [(offset, duration, [])]

        # timestep, score, and relevant positions. Include first position as it is skipped in iteration
        flying_relevant_spaces = [(offset, duration, [(0.0, (craft_path[0][X], craft_path[0][Y]))])]

        # Counts for move types
        angular = 0
        straight = 0
        alt_changes = 0

        # Setup duration values - dependent on ingress/egress, differentiated by offset (0 is ingress, otherwise egress)
        a_dur = self.C_ANGLED_DUR * self.weights[self.CRAFT_FLYING_INGRESS]
        s_dur = self.C_STRAIGHT_DUR * self.weights[self.CRAFT_FLYING_INGRESS]
        alt_dur = self.C_ALT_CHANGE_DUR * self.weights[self.CRAFT_FLYING_INGRESS]
        day = self.C_DAY_DUR * self.weights[self.CRAFT_FLYING_INGRESS]

        if offset > 0:
            a_dur = self.C_ANGLED_DUR * self.weights[self.CRAFT_FLYING_EGRESS]
            s_dur = self.C_STRAIGHT_DUR * self.weights[self.CRAFT_FLYING_EGRESS]
            alt_dur = self.C_ALT_CHANGE_DUR * self.weights[self.CRAFT_FLYING_EGRESS]
            day = self.C_DAY_DUR * self.weights[self.CRAFT_FLYING_EGRESS]

        for timestep, pos in enumerate(craft_path[1:]):    # Already have first heading, go through rest of the path
            alt_changed = False
            straight_move = True

            if pos[H] in self.straight_moves:   # Check if craft made a straight move
                straight += 1
            else:   # Otherwise, craft heading is different and craft has made an angular move
                angular += 1
                straight_move = False

            if prev_alt != pos[2]:    # If altitude is not equivalent, then there has been a change
                alt_changes += 1
                alt_changed = True

            # Update values
            prev_alt = pos[Z]

            # Compute cumulative duration for relevant spaces
            duration = s_dur*straight + a_dur*angular + alt_dur*alt_changes

            # Compute local position duration for relevant spaces
            local = s_dur if straight_move else a_dur
            if alt_changed:
                local += alt_dur

            # Check for crash
            # First, for crash inside AOI - make sure that there is no index out of bounds error
            inside = self.engine.inside_AOI(pos[X], pos[Y])
            if inside and self.engine.elevation_map[pos[Y]][pos[X]] >= pos[Z]:
                duration += self.C_DAY_DUR # Minutes in a day
                local += day
                crash = True
            elif not inside:    # Crash if leave AOI (would cause index out of bounds)
                duration += day # Minutes in a day
                local += day
                crash = True

            # Store all spaces that contributed to score in list
            flying_relevant_spaces.append((timestep+offset, duration, [(local, (pos[X], pos[Y]))]))

            if crash:
                break

        return duration, flying_relevant_spaces

    def score_hiker_difficulty(self, hiker_pos, drop_pos, drop_time):
        """
        Calculate duration of hiker route to package using A* in terms of minutes (line of sight not included).
        See Document Game Rule 2
        parameters:
            hiker_pos (tuple): size 4 tuple with (x, y, z, heading)
            drop_pos (tuple): size 4 tuple with (x, y, z, heading).  Represents package land location.
            drop_time (int): timestep that the drop occurred.  Used for spaces timestep offset.
        returns:
            hiker_river_crossing (float): Number of minutes the hiker takes to cross some body of water
                                          to reach the package.
            hiker_climbing (float): Number of minutes the hiker takes to climb up or down terrain
                                    while hiking to the package.
            hiker_route_duration (float): Number of minutes the hiker takes to hike across flat terrain
                                          to reach the package (2 dimensions).
            hiker_river_positions (list):  Positions that affect hiker river crossing duration
            hiker_climbing_positions (list): Positions that affect hiker climbing duration
            hiker_route_positions (list): Positions that affect hiker route length duration
        """

        self.engine.logger.debug("Hiker moving from initial {} to package at {}".format(hiker_pos, drop_pos))

        # Initialize values
        hiker_river_crossing = 0.0
        hiker_forest_delay = 0.0
        hiker_climbing = 0.0
        hiker_route_duration = 0.0

        # Positions relevant at timestep of drop
        hiker_river_positions = []
        hiker_forest_positions = []
        hiker_climbing_positions = []
        hiker_route_positions = []

        # Relevant spaces across timesteps - [[timestep, score_at_timestep, [[score_at_space, (x, y)], ...]], ...]
        hiker_river_relevant_spaces = [(drop_time, hiker_river_crossing, [])]
        hiker_forest_relevant_spaces = [(drop_time, hiker_forest_delay, [])]
        hiker_climbing_relevant_spaces = [(drop_time, hiker_climbing, [])]
        hiker_route_relevant_spaces = [(drop_time, hiker_route_duration, [])]

        # If not dropped, return default values.
        if drop_pos is None:
            return hiker_river_crossing, hiker_forest_delay, hiker_climbing, hiker_route_duration, \
                hiker_river_relevant_spaces, hiker_forest_relevant_spaces, hiker_climbing_relevant_spaces, hiker_route_relevant_spaces

        # Route obstacle counts
        straight = 0
        angled = 0
        rivers_in_path = 0
        forest_in_path = 0
        changes_in_elevation = 0

        # Get path from hiker to package land position
        hiker_path, got_there = self.hiker_pathfind(hiker_pos, drop_pos)
        self.found = got_there
        self.hiker_path = hiker_path

        self.engine.logger.debug("Hiker path ({}): {}".format(self.found, hiker_path))

        if self.found:
            old_z = hiker_pos[Z]    # Hiker altitude

            # Iterate through path to calculate penalty
            # Avoid the first position (Hiker position) because cannot
            # assign penalty for starting pos (off by 1 error)
            for pos in hiker_path[1:]:
                x, y, z, h = pos
                tile = self.engine.tile_map[y][x]

                # Check if hiker move is angled or straight
                if h in self.straight_moves:
                    straight += 1
                    # Append positions to relevant hiker path
                    hiker_route_positions.append((self.H_STRAIGHT, (x, y)))
                else:
                    angled += 1
                    # Append positions to relevant hiker path
                    hiker_route_positions.append((self.H_ANGLE, (x, y)))
                # Check if this space is a river
                if tile == TTypes.RIVER:
                    rivers_in_path += 1
                    hiker_river_positions.append((self.H_RIVER, (x, y)))
                if tile == TTypes.FOREST:
                    forest_in_path += 1
                    hiker_forest_positions.append((self.H_FOREST, (x, y)))
                # Check if there is an elevation change
                if z != old_z:
                    changes_in_elevation += 1
                    old_z = z
                    hiker_climbing_positions.append((self.H_ELEVATION, (x, y)))
                
            # Calculate durations
            hiker_route_duration = (self.H_STRAIGHT*straight + self.H_ANGLE*angled)
            hiker_river_crossing = self.H_RIVER*rivers_in_path
            hiker_forest_delay = self.H_FOREST*forest_in_path
            hiker_climbing = self.H_ELEVATION*changes_in_elevation
        else:
            self.engine.logger.error("Hiker unable to reach the package!")

        # Multiple spaces affect a single timestep
        hiker_river_relevant_spaces = [(drop_time, hiker_river_crossing, hiker_river_positions)]
        hiker_forest_relevant_spaces = [(drop_time, hiker_forest_delay, hiker_forest_positions)]
        hiker_climbing_relevant_spaces = [(drop_time, hiker_climbing, hiker_climbing_positions)]
        hiker_route_relevant_spaces = [(drop_time, hiker_route_duration, hiker_route_positions)]

        return hiker_river_crossing, hiker_forest_delay, hiker_climbing, hiker_route_duration, \
            hiker_river_relevant_spaces, hiker_forest_relevant_spaces, hiker_climbing_relevant_spaces, hiker_route_relevant_spaces

    def score_package_los(self, hiker_pos, drop_pos, drop_time):
        """
        Plot a straight line from hiker pos to drop pos, finding which obstacles intersect that line
        (integer raytracing equation based on Bresenham line algorithm)

        For example, calculate which grid positions intersect a line with slope 3/5
        (Hiker at (0, 4), package at (5, 1))

        |---|---|---|---|---|
        |   |   |   |   |   |
        |---|---|---|---|---|
        |   |   |   ||||||P||
        |---|---|---|---|---|
        |   |||||||||||||   |
        |---|---|---|---|---|
        ||H||||||   |   |   |
        |---|---|---|---|---|

        See Document Game Rule 2
        Reference: https://blog.demofox.org/2015/01/17/bresenhams-drawing-algorithms/
        parameters:
            hiker_pos (tuple): size 4 tuple with (x, y, z, heading)
            drop_pos (tuple): size 4 tuple with (x, y, z, heading).  Represents package land location.
            drop_time (int): timestep that the drop occurred.  Used for spaces timestep offset.
        returns:
            duration (float): Number of additional minutes taken while hiking to a package due
                              to searching past high elevation obstacles.
            los_relevant_spaces (list): duration per space breakdown
        """

        los_positions = []  # Spaces that intersect with the hiker's line of sight
        los_spaces = []   # Relevant positions that intersect with hiker's line of sight
        los_relevant_positions = []  # Relevant spaces - [[timestep, score_at_timestep, [[score_at_space, (x, y)], ...]], ...]
        duration = 0.0  # Total duration of delay due to LoS obstacles

        if drop_pos is None:
            return duration, [(drop_time, duration, [])]

        # Drone dropped on hiker (xy matches)
        if drop_pos[X] == hiker_pos[X] and drop_pos[Y] == hiker_pos[Y]:
            self.engine.logger.debug("Dropped over hiker ({}) at {}".format(hiker_pos, drop_pos))
            return duration, [(drop_time, duration, [(duration, (drop_pos[X], drop_pos[Y]))])]

        # Beam width
        delta_x = int(abs(drop_pos[X] - hiker_pos[X]))
        delta_y = int(abs(drop_pos[Y] - hiker_pos[Y]))
        n = 1 + delta_x + delta_y   # How far along the line are we?

        # Determine direction of increment (which way is the line pointing?)
        x_increment = 1 if (drop_pos[X] > hiker_pos[X]) else -1
        y_increment = 1 if (drop_pos[Y] > hiker_pos[Y]) else -1
        error = delta_x - delta_y   # Allowable distance for "edge case coordinate spaces"

        # Amount to swing between x increment and y increment
        delta_x *= 2
        delta_y *= 2
        
        line_x, line_y, line_z, line_h = hiker_pos
        for _ in range(n, 0, -1):  # Decrement until done (reach 0)
            los_positions.append((int(line_x), int(line_y)))
            if error > 0:   # Calculate next position to include in positions (account for slope)
                line_x += x_increment
                error -= delta_y
            else:
                line_y += y_increment
                error += delta_x

        # Process LOS path
        for lpos in los_positions:
            lpos_x, lpos_y = lpos
            # Only process spaces within AOI
            if self.engine.inside_AOI(lpos_x, lpos_y):
                tile_type = self.engine.tile_map[lpos_y][lpos_x]
                self.engine.logger.debug("LOS Position ({}) has {}".format(lpos, tile_type))

                if tile_type in self.elevation_obs:
                    duration += self.H_LOS_DELAY
                    los_spaces.append((self.H_LOS_DELAY, lpos))
                else:
                    # no penalty, but still add to help visualize line of sight path
                    los_spaces.append((0.0, lpos))

        los_relevant_spaces = [(drop_time, duration, los_spaces)]  # Single timestep, multiple positions

        return duration, los_relevant_spaces

    def score_package_risk(self, hiker_pos, drop_point, drop_time):
        """
        Calculate penalty of package drop. See Document Game Rule 4
        Penalty = 1 * (obs) / # of tiles in drop zone
        parameters:
            drop_point (tuple): size 4 tuple with (x, y, z, heading).
                                Represents craft position at time of drop.
            land_pos (tuple): size 4 tuple with (x, y, z, heading).
                              Represents landing position of package
            drop_time (int): timestep that the drop occurred.  Used for spaces timestep offset.
                             Note that drop time does not matter to Klarigo if drop_point is None.
        returns:
            dmg_penalty (float): Penalty for dropping the package where it is at risk of being damaged.
            inac_penalty (float): Penalty for dropping with a possible landing position
                                  inaccessible to the hiker.
            duration (float): Delay added to hiker route due to difficulty of accessing the package
                              at landing point.
            dmg_relevant_spaces (list): penalty per space breakdown
            inaccess_relevant_spaces (list): penalty per space breakdown
            duration_relevant_spaces (list): duration per space breakdown
        """

        # Penalty and duration
        dmg_penalty = 0.0
        inac_penalty = 0.0

        # If there is no drop point, return.
        if drop_point is None:
            return self.P_DMG, self.P_OUT_OF_REACH, \
                [(drop_time, self.P_DMG, [])], [(drop_time, self.P_OUT_OF_REACH, [])]

        drop_x, drop_y, drop_z, drop_h = drop_point

        # Relevant spaces
        dmg_positions = []
        inac_positions = []

        drop_point_reachable = True

        if drop_z == 1: # dropped at altitude 1, 0 package scatter
            if self.engine.inside_AOI(drop_x, drop_y):
                tile = self.engine.tile_map[drop_y][drop_x]
                
                # Check for damage penalty
                if tile == TTypes.FOREST:   # Tree
                    dmg_penalty = self.P_DMG_TREE
                    dmg_positions.append((self.P_DMG_TREE, (drop_x, drop_y)))
                elif tile == TTypes.RIVER: # River
                    dmg_penalty = self.P_DMG_WATER
                    dmg_positions.append((self.P_DMG_WATER, (drop_x, drop_y)))
                elif tile in self.buildings:  # Buildings
                    dmg_penalty = self.P_DMG_BUILDING
                    dmg_positions.append((self.P_DMG_BUILDING, (drop_x, drop_y)))
                else:   # Append values of 0 for showing drop area
                    dmg_positions.append((0.0, (drop_x, drop_y)))

                # Check for inaccessibility delays
                # Check if hiker can get to position
                land_z = self.engine.elevation_map[drop_y][drop_x]
                if tile == TTypes.FOREST:
                    land_z = 0

                h_path, h_found = self.hiker_pathfind(hiker_pos, (drop_x, drop_y, land_z, 1))
                if not h_found:
                    drop_point_reachable = False
                    inac_penalty = self.P_OUT_OF_REACH
                    inac_positions.append((self.P_OUT_OF_REACH, (drop_x, drop_y)))
                else:    # Append values of 0 for showing drop area
                    if tile == TTypes.RIVER:
                        inac_penalty = self.P_ACCESS_WATER
                    elif tile == TTypes.FOREST:
                        inac_penalty = self.P_ACCESS_FOREST

                    inac_positions.append((inac_penalty, (drop_x, drop_y)))
            else:
                inac_penalty = self.P_OUT_OF_REACH
                
        elif drop_z == 2: # dropped at altitude 2, 1 space radius package scatter
            for dx in range(drop_x-1, drop_x+2):
                for dy in range(drop_y-1, drop_y+2):
                    if self.engine.inside_AOI(dx, dy):  # Only give penalty for what is inside AOI
                        tile = self.engine.tile_map[dy][dx]

                        # Check for damage penalty
                        if tile == TTypes.FOREST:
                            dmg_penalty += self.P_DMG_TREE
                            dmg_positions.append((self.P_DMG_TREE, (dx, dy)))
                        elif tile == TTypes.RIVER: # River
                            dmg_penalty += self.P_DMG_WATER
                            dmg_positions.append((self.P_DMG_WATER, (dx, dy)))
                        elif tile in self.buildings:
                            dmg_penalty += self.P_DMG_BUILDING
                            dmg_positions.append((self.P_DMG_BUILDING, (dx, dy)))
                        else:
                            # Append values of 0 for showing drop area
                            dmg_positions.append((0.0, (dx, dy)))

                        # Check for inaccessibility delays
                        dz = self.engine.elevation_map[dy][dx]
                        if tile == TTypes.FOREST:
                            dz = 0

                        h_path, h_found = self.hiker_pathfind(hiker_pos, (dx, dy, dz, 1))
                        if not h_found:
                            if dx == drop_x and dy == drop_y:
                                drop_point_reachable = False
                            inac_penalty += self.P_OUT_OF_REACH
                            inac_positions.append((self.P_OUT_OF_REACH, (dx, dy)))
                        else:
                            local_penalty = 0
                            if tile == TTypes.RIVER:
                                inac_penalty += self.P_ACCESS_WATER
                                local_penalty = self.P_ACCESS_WATER
                            elif tile == TTypes.FOREST:
                                inac_penalty += self.P_ACCESS_FOREST
                                local_penalty = self.P_ACCESS_FOREST
                            inac_positions.append((local_penalty, (dx, dy)))
                    else:
                        inac_penalty += self.P_OUT_OF_REACH

            dmg_penalty = dmg_penalty / self.P_ALT_2_SPACES   # Get average across scatter zone
            inac_penalty = inac_penalty / self.P_ALT_2_SPACES
        elif drop_z == 3: # dropped at altitude 3, 2 space radius package scatter
            for dx in range(drop_x-2, drop_x+3):
                for dy in range(drop_y-2, drop_y+3):
                    if self.engine.inside_AOI(dx, dy):  # Only give penalty for what is inside AOI
                        tile = self.engine.tile_map[dy][dx]

                        # Check for damage penalty
                        if tile == 1:
                            dmg_penalty += self.P_DMG_TREE
                            dmg_positions.append((self.P_DMG_TREE, (dx, dy)))
                        elif tile == TTypes.RIVER: # River
                            dmg_penalty += self.P_DMG_WATER
                            dmg_positions.append((self.P_DMG_WATER, (dx, dy)))
                        elif tile in self.buildings:
                            dmg_penalty += self.P_DMG_BUILDING
                            dmg_positions.append((self.P_DMG_BUILDING, (dx, dy)))
                        else:
                            dmg_positions.append((0.0, (dx, dy)))   # Append values of 0 for showing drop area

                        # Check for inaccesibility delays
                        dz = self.engine.elevation_map[dy][dx]
                        if tile == TTypes.FOREST:
                            dz = 0

                        h_path, h_found = self.hiker_pathfind(hiker_pos, (dx, dy, dz, 1))
                        if not h_found:
                            if dx == drop_x and dy == drop_y:
                                drop_point_reachable = False
                            inac_penalty += self.P_OUT_OF_REACH
                            inac_positions.append((self.P_OUT_OF_REACH, (dx, dy)))
                        else:
                            local_penalty = 0
                            if tile == TTypes.RIVER:
                                inac_penalty += self.P_ACCESS_WATER
                                local_penalty = self.P_ACCESS_WATER
                            elif tile == TTypes.FOREST:
                                inac_penalty += self.P_ACCESS_FOREST
                                local_penalty = self.P_ACCESS_FOREST
                            inac_positions.append((local_penalty, (dx, dy)))
                    else:
                        inac_penalty += self.P_OUT_OF_REACH
        
            dmg_penalty = dmg_penalty / self.P_ALT_3_SPACES   # Get average across scatter zone
            inac_penalty = inac_penalty / self.P_ALT_3_SPACES

        if not drop_point_reachable:
            inac_penalty = self.P_DMG

        return dmg_penalty, inac_penalty, [(drop_time, dmg_penalty, dmg_positions)], \
            [(drop_time, inac_penalty, inac_positions)]

    def score_craft_safety(self, craft_path, offset):
        """
        See Document Game Rule 3
        parameters:
            craft_path (list): list of size 4 tuples (x, y, z, heading) that represents
                               states of a craft in flight
            offset (int): time offset for differentiating between different flight segments (ingress/egress)
        returns:
            penalty (float): penalty for flying too close to obstacles with a higher elevation than the craft
            safety_relevant_spaces (list): Spaces that caused scores
                                           [[timestep, score_at_timestep, [[score_at_space, (x, y)], ...]], ...]
        """

        penalty = 0.0
        total = 0.0       # Checksum for penalty

        safety_relevant_spaces = [] # timestep, score, and positions relevant to safety score at timestep

        # Setup penalty values - dependent on ingress/egress, differentiated by offset (0 is ingress, otherwise egress)
        safety_penalty = self.C_SAFETY_RISK * self.weights[self.CRAFT_SAFETY_INGRESS]
        crash_penalty = self.C_CRASH_PENALTY * self.weights[self.CRAFT_SAFETY_INGRESS]

        if offset > 0:
            safety_penalty = self.C_SAFETY_RISK * self.weights[self.CRAFT_SAFETY_EGRESS]
            crash_penalty = self.C_CRASH_PENALTY * self.weights[self.CRAFT_SAFETY_EGRESS]

        # Iterate through list of length 4 tuples
        for timestep, pos in enumerate(craft_path):
            x, y, z, h = pos
            sides = []

            if h == 1:    # 1 = North, 8 total headings
                try:
                    sides.append((self.engine.elevation_map[y][x-1], (x-1, y)))    # 7 (Left)
                except IndexError:  # Craft has flown too close to world edge
                    pass
                try:
                    sides.append((self.engine.elevation_map[y-1][x-1], (x-1, y-1)))  # 8 (Left Diagonal)
                except IndexError:
                    pass
                try:
                    sides.append((self.engine.elevation_map[y-1][x], (x, y-1)))    # 1 (Forward)
                except IndexError:
                    pass
                try:
                    sides.append((self.engine.elevation_map[y-1][x+1], (x+1, y-1)))  # 2 (Right Diagonal)
                except IndexError:
                    pass
                try:
                    sides.append((self.engine.elevation_map[y][x+1], (x+1, y)))    # 3 (Right)
                except IndexError:
                    pass
            elif h == 2:    # 2 = Northeast
                try:
                    sides.append((self.engine.elevation_map[y-1][x-1], (x-1, y-1)))  # 8 (Left)
                except IndexError:
                    pass
                try:
                    sides.append((self.engine.elevation_map[y-1][x], (x, y-1)))    # 1 (Left Diagonal)
                except IndexError:
                    pass
                try:
                    sides.append((self.engine.elevation_map[y-1][x+1], (x+1, y-1)))  # 2 (Forward)
                except IndexError:
                    pass
                try:
                    sides.append((self.engine.elevation_map[y][x+1], (x+1, y)))    # 3 (Right Diagonal)
                except IndexError:
                    pass
                try:
                    sides.append((self.engine.elevation_map[y+1][x+1], (x+1, y+1)))  # 4 (Right)
                except IndexError:
                    pass
            elif h == 3:    # 3 = East
                try:
                    sides.append((self.engine.elevation_map[y-1][x], (x, y-1)))    # 1 (Left)
                except IndexError:
                    pass
                try:
                    sides.append((self.engine.elevation_map[y-1][x+1], (x+1, y-1)))  # 2 (Left Diagonal)
                except IndexError:
                    pass
                try:
                    sides.append((self.engine.elevation_map[y][x+1], (x+1, y)))    # 3 (Forward)
                except IndexError:
                    pass
                try:
                    sides.append((self.engine.elevation_map[y+1][x+1], (x+1, y+1)))  # 4 (Right Diagonal)
                except IndexError:
                    pass
                try:
                    sides.append((self.engine.elevation_map[y+1][x], (x, y+1)))    # 5 (Right)
                except IndexError:
                    pass
            elif h == 4:    # 4 = Southeast
                try:
                    sides.append((self.engine.elevation_map[y-1][x+1], (x+1, y-1)))  # 2 (Left)
                except IndexError:
                    pass
                try:
                    sides.append((self.engine.elevation_map[y][x+1], (x+1, y)))    # 3 (Left Diagonal)
                except IndexError:
                    pass
                try:
                    sides.append((self.engine.elevation_map[y+1][x+1], (x+1, y+1)))  # 4 (Forward)
                except IndexError:
                    pass
                try:
                    sides.append((self.engine.elevation_map[y+1][x], (x, y+1)))    # 5 (Right Diagonal)
                except IndexError:
                    pass
                try:
                    sides.append((self.engine.elevation_map[y+1][x-1], (x-1, y+1)))  # 6 (Right)
                except IndexError:
                    pass
            elif h == 5:    # 5 = South
                try:
                    sides.append((self.engine.elevation_map[y][x+1], (x+1, y)))    # 3 (Left)
                except IndexError:
                    pass
                try:
                    sides.append((self.engine.elevation_map[y+1][x+1], (x+1, y+1)))  # 4 (Left Diagonal)
                except IndexError:
                    pass
                try:
                    sides.append((self.engine.elevation_map[y+1][x], (x, y+1)))    # 5 (Forward)
                except IndexError:
                    pass
                try:
                    sides.append((self.engine.elevation_map[y+1][x-1], (x-1, y+1)))  # 6 (Right Diagonal)
                except IndexError:
                    pass
                try:
                    sides.append((self.engine.elevation_map[y][x-1], (x-1, y)))    # 7 (Right)
                except IndexError:
                    pass
            elif h == 6:    # 6 = Southwest
                try:
                    sides.append((self.engine.elevation_map[y+1][x+1], (x+1, y+1)))  # 4 (Left)
                except IndexError:
                    pass
                try:
                    sides.append((self.engine.elevation_map[y+1][x], (x, y+1)))    # 5 (Left Diagonal)
                except IndexError:
                    pass
                try:
                    sides.append((self.engine.elevation_map[y+1][x-1], (x-1, y+1)))  # 6 (Forward)
                except IndexError:
                    pass
                try:
                    sides.append((self.engine.elevation_map[y][x-1], (x-1, y)))    # 7 (Right Diagonal)
                except IndexError:
                    pass
                try:
                    sides.append((self.engine.elevation_map[y-1][x-1], (x-1, y-1)))  # 8 (Right)
                except IndexError:
                    pass
            elif h == 7:    # 7 = West
                try:
                    sides.append((self.engine.elevation_map[y+1][x], (x, y+1)))    # 5 (Left)
                except IndexError:
                    pass
                try:
                    sides.append((self.engine.elevation_map[y+1][x-1], (x-1, y+1)))  # 6 (Left Diagonal)
                except IndexError:
                    pass
                try:
                    sides.append((self.engine.elevation_map[y][x-1], (x-1, y)))    # 7 (Forward)
                except IndexError:
                    pass
                try:
                    sides.append((self.engine.elevation_map[y-1][x-1], (x-1, y-1)))  # 8 (Right Diagonal)
                except IndexError:
                    pass
                try:
                    sides.append((self.engine.elevation_map[y-1][x], (x, y-1)))    # 1 (Right)
                except IndexError:
                    pass
            elif h == 8:    # 8 = Northwest
                try:
                    sides.append((self.engine.elevation_map[y+1][x-1], (x-1, y+1)))  # 6 (Left)
                except IndexError:
                    pass
                try:
                    sides.append((self.engine.elevation_map[y][x-1], (x-1, y)))    # 7 (Left Diagonal)
                except IndexError:
                    pass
                try:
                    sides.append((self.engine.elevation_map[y-1][x-1], (x-1, y-1)))  # 8 (Forward)
                except IndexError:
                    pass
                try:
                    sides.append((self.engine.elevation_map[y-1][x], (x, y-1)))    # 1 (Right Diagonal)
                except IndexError:
                    pass
                try:
                    sides.append((self.engine.elevation_map[y-1][x+1], (x+1, y-1)))  # 2 (Right)
                except IndexError:
                    pass

            penalty_pos = []    # List for keeping penalty positions for this timestep

            # Check each side to see if it flew close to dangerous obstacles
            for side, spos in sides:
                # If elevation at side is greater than or equal to altitude at this position, increase penalty
                if side >= z:
                    penalty_pos.append((safety_penalty, spos))
                    penalty += safety_penalty

            # Use checksum - only if penalty has changed add an entry
            if total != penalty:
                # add timestep + relevant positions
                safety_relevant_spaces.append((timestep+offset, penalty, penalty_pos))
                total = penalty

            # See Document Game Rules 3 - If there is a crash on egress,
            # (after not crashing on ingress) then penalty can be applied.  Cannot be applied to both
            if offset > 0:
                # If a crash has been recorded, then increase penalty (dont include exiting AOI)
                if self.engine.inside_AOI(x, y) and self.engine.elevation_map[y][x] >= z:
                    penalty += crash_penalty
                elif not self.engine.inside_AOI(x, y):
                    penalty += crash_penalty

        return penalty, safety_relevant_spaces

    def hiker_pathfind(self, hiker_pos, drop_pos):
        # Custom heuristic for hiker - higher cost for going through rivers or mountains
        def heuristic(cur_x, cur_y, end_x, end_y):
            return self.H_MOVE_S * euclidean_distance(cur_x, cur_y, end_x, end_y)

        # Use A* search to get path
        came_from = {}  # Dictionary with key:value -> position: path to position
        cost_so_far = {}    # Dictionary with key:value -> position: cost of path to position
        frontier = []   # Frontier priority queue
        final_path = [] # Final path of hiker
        current = hiker_pos  # Current hiker position

        # Init starting pos
        found = False
        came_from[current] = [current]
        cost_so_far[current] = 0
        heapq.heappush(frontier, (0, current))

        # unpack end position
        end_x, end_y, end_z, end_h = drop_pos

        # Solve A*
        while len(frontier) != 0:
            current = heapq.heappop(frontier)[1]
            x, y, z, h = current

            # Early exit - if hiker xyz == package xyz
            if current[:3] == drop_pos[:3]:
                found = True
                break

            # Expand - hiker can move in 8 cardinal directions
            for move in self.hiker_moves:
                next_x, next_y, next_h = move
                # get next move coordinates by adding current coordinates to relative move coordinates
                next_x += x
                next_y += y
                if self.engine.inside_AOI(next_x, next_y):
                    # If tile is within AOI, can consider expanding
                    tile = self.engine.tile_map[next_y][next_x]
                    next_z = self.engine.elevation_map[next_y][next_x]
                    if tile == TTypes.FOREST:
                        next_z = 0
                    if tile in self.hiker_access:    # If tile can be moved through, expand
                        next_move = (next_x, next_y, next_z, next_h)
                        h = heuristic(next_x, next_y, end_x, end_y)

                        # Do route cost - assign cost according to straight vs angled move
                        move_cost = self.H_MOVE_S if next_move[H] in self.straight_moves else self.H_MOVE_A
                        if tile == TTypes.RIVER:
                            move_cost += self.H_MOVE_RIVER
                        elif tile == TTypes.FOREST:
                            move_cost += self.H_MOVE_FOREST

                        if next_z != z:
                            move_cost += self.H_MOVE_ELEVATION
                        
                        next_cost = move_cost + cost_so_far[current]

                        # Expand if neighbor has not been visited before or if new cost is cheaper
                        if next_move not in cost_so_far or next_cost < cost_so_far[next_move]:
                            # Update path to neigboring position
                            next_path = copy.copy(came_from[current])
                            next_path.append(next_move)
                            came_from[next_move] = next_path

                            cost_so_far[next_move] = next_cost   # Update cost of position
                            heapq.heappush(frontier, (h + next_cost, next_move))

        # If the hiker is able to reach the package
        if found:
            # Due to 4 dimensions xyzh, possible that multiple paths were found with different headings.
            # Try all headings and select result path that costs the least.
            for heading in range(1, 9):
                end_position = (drop_pos[X], drop_pos[Y], drop_pos[Z], heading)

                # Skip heading if the end position has not actually been visited
                if end_position not in came_from:
                    continue

                # Fetch path associated with heading and its cost
                a_path = came_from[end_position]
                # fetch cost associated with last position in path (cost to reach position)
                a_path_cost = cost_so_far[a_path[-1]]

                # Fetch final path cost
                # cost is initially infinite such that whatever path that comes next is always favored
                final_path_cost = cost_so_far[final_path[-1]] if len(final_path) != 0 else float('inf')

                self.engine.logger.debug("Possible path is: {}".format(a_path))

                # Compare and take the least costing path.
                # Only time that final path length is 0 is first iteration (initial value is [])
                if len(final_path) == 0 or a_path_cost < final_path_cost:
                    final_path = a_path
        
        return final_path, found
