import json
import math
import enum
import os

# Config
DIRECTORY = os.path.dirname(os.path.abspath(__file__))
VARIANT_CONFIG = 'variant_config.json'
VARIANT_CONFIG_PATH = os.path.join(DIRECTORY, VARIANT_CONFIG)


# Direction Enums
class Dirs(enum.IntEnum):
    NORTH = 1
    NORTHEAST = 2
    EAST = 3
    SOUTHEAST = 4
    SOUTH = 5
    SOUTHWEST = 6
    WEST = 7
    NORTHWEST = 8


# Tiletype Enums
class TTypes(enum.IntEnum):
    FOREST = 1
    GRASS = 2
    ROAD = 10
    AIRTRAFFIC_TOWER = 13
    RIVER = 15
    FIREWATCH_TOWER = 17
    LOW_HILL = 24
    HILL = 25
    FOOTHILL = 26
    MOUNTAIN = 31


def euclidean_distance(x1, y1, x2, y2):
    return math.sqrt((x1 - x2) ** 2 + (y1 - y2) ** 2)


def euclidean_distance_3(x1, y1, z1, x2, y2, z2):
    return math.sqrt((x1 - x2) ** 2 + (y1 - y2) ** 2 + (z1 - z2) ** 2)


def get_penalty_weights(schema):
    if type(schema) is not str and type(schema) is not int:
        raise ValueError("Cannot fetch penalty weights for invalid schema: {} ({})".format(schema, type(schema)))

    with open(VARIANT_CONFIG_PATH, 'r') as f:
        schemas = json.load(f)

        for s, vals in schemas.items():
            for variant in vals:
                if variant['schema'] == int(schema):
                    return variant['weights']
        else:
            raise ValueError("No weights for schema {}".format(schema))
