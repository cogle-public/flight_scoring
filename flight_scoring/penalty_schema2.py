import math
import random
import heapq
import json
import copy

from .scoring_utils import euclidean_distance


class Schema2:
    def __init__(self, engine, schema_num=2):
        self.engine = engine
        self.schema_num = schema_num

    def init_factors(self):
        pass

    def set_schema_num(self, schema_num):
        if type(schema_num) is int:
            self.schema_num = schema_num

    def get_factors(self):
        return self.variants[self.variant]

    def get_narrative(self, penalties):
        narrative = 'PLACEHOLDER'

        return narrative

    def get_comp_narrative(self, penalty1, penalty2, commander, constraints):
        narrative = 'PLACEHOLDER'

        return 'Lorem', 'Ip', 'Sum'

    def calculate(self, craft_path, drop, partial_flight):
        # Calculate score

        hiker_river_crossing = 0.0
        hiker_climbing = 0.0
        hiker_package_los = 0.0
        hiker_route_length = 0.0
        package_damage = 0.0
        package_inaccess = 0.0
        spaces_river = []
        spaces_climbing = []
        spaces_los = []
        spaces_route = []
        spaces_damage = []
        spaces_inaccess = []

        before_drop = []
        after_drop = []
        package_land = drop
        drop_index = -1
        
        for i, pos in enumerate(craft_path):
            if pos == drop:
                drop_index = i
                break
        if drop_index != -1:
            drop_index = drop_index+1 if len(craft_path)-1 >= drop_index else drop_index

            before_drop = craft_path[:drop_index+1]
            after_drop = craft_path[drop_index:]
        else:
            before_drop = craft_path

        craft_destination_error, craft_leave_error, spaces_destination, spaces_leave = self.score_craft_performance(self.engine.end, craft_path)

        craft_safety_ingress, spaces_safety_ingress = self.score_craft_safety(before_drop, 0)
        craft_distance_ingress, spaces_distance_ingress = self.score_craft_distance(before_drop, 0)

        if drop is not None:
            # planned drop target
            drop_target = (drop[0], drop[1], self.engine.elevation_map[drop[1]][drop[0]], drop[3])

            hiker_river_crossing, hiker_climbing, hiker_route_length, spaces_river, spaces_climbing, spaces_route = self.score_hiker_difficulty(self.engine.hiker, drop_target, drop_index-1)
            # print("Package LOS")
            hiker_package_los, spaces_los = self.score_package_los(self.engine.hiker, drop, drop_index-1)

            # print("scoring package safety")
            # Grab elevation of craft at point of drop
            package_damage, package_inaccess, spaces_damage, spaces_inaccess = self.score_package_safety(drop_target, drop_index)

        # print("scoring cse")
        craft_safety_egress, spaces_safety_egress = self.score_craft_safety(after_drop, drop_index-1)

        # print("scoring cde")
        craft_distance_egress, spaces_distance_egress = self.score_craft_distance(after_drop, drop_index-1)

        total = sum([craft_safety_ingress, craft_distance_ingress, hiker_river_crossing,
                     hiker_climbing, hiker_route_length, hiker_package_los, package_damage,
                     package_inaccess, craft_safety_egress, craft_distance_egress,
                     craft_destination_error, craft_leave_error])

        # print("TOTAL: {}".format(total))

        # Dictionary of spaces relavent to the scoring
        spaces_dict = {
            "ced": spaces_destination,
            "cel": spaces_leave,
            "csi": spaces_safety_ingress,
            "cdi": spaces_distance_ingress,
            "hrc": spaces_river,
            "hc": spaces_climbing,
            "hrl": spaces_route,
            "hlos": spaces_los,
            "cse": spaces_safety_egress,
            "cde": spaces_distance_egress,
            "pd": spaces_damage,
            "pi": spaces_inaccess
        }

        # Schema 2
        return {
            "ced": craft_destination_error,
            "cel": craft_leave_error,
            "csi": craft_safety_ingress,
            "cdi": craft_distance_ingress,
            "hrc": hiker_river_crossing,
            "hc": hiker_climbing,
            "hrl": hiker_route_length,
            "hlos": hiker_package_los,
            "cse": craft_safety_egress,
            "cde": craft_distance_egress,
            "pd": package_damage,
            "pi": package_inaccess,
            "total": total,
            "spaces": json.dumps(spaces_dict),
            "schema": self.schema_num
        }

    # destination penalty = 3d euclidean distance from final to intended destination, leave penalty = 200 if craft out of bounds
    # NOTE: Possible race condition that GLOBAL_POS_INT message does not arrive before the end, causing last position to not be recorded by hybrid agent in add_to_flight_path
    def score_craft_performance(self, end_point, craft_path):
        dest_penalty = 0.0
        leave_penalty = 0.0

        craft_x = craft_path[-1][0]
        craft_y = craft_path[-1][1]
        craft_z = craft_path[-1][2]

        dest_x = end_point[0]
        dest_y = end_point[1]
        dest_z = end_point[2]

        if craft_x != dest_x or craft_y != dest_y:
            distance = self.engine.euclidean_distance_3(craft_x, craft_y, craft_z, dest_x, dest_y, dest_z)

            if distance < 3:
                dest_penalty += distance
            else:
                dest_penalty += 20.0*distance
        
        if dest_penalty > 200.0:
            dest_penalty = 200.0

        if not self.engine.inside_AOI(craft_x, craft_y):   # Penalize for leaving AOI
            leave_penalty += 200.0

        # Time is at -3 due to the first move not counting and last noop
        dest_relavent_spaces = [(len(craft_path)-3, dest_penalty, (dest_penalty, (craft_x, craft_y)))]  # Timestep and score/relavent space (endpoint) pair
        leave_relavent_spaces = [(len(craft_path)-3, leave_penalty, (leave_penalty, (craft_x, craft_y)))]  # Timestep and score/relavent space (endpoint) pair

        return dest_penalty, leave_penalty, dest_relavent_spaces, leave_relavent_spaces

    def inside_AOI(self, x, y):
        return x >= 0 and x < self.engine.AOI[2] and y >= 0 and y < self.engine.AOI[3]

    # Per segment in path: 1*(segments where spaces relative left through front through right of craft (180 front degrees) are >= alt) + (200*number of crashes)
    def score_craft_safety(self, craft_path, offset):
        penalty = 0
        total = 0       # Checksum for penalty

        end_index = len(craft_path) - 1

        saftey_relavent_spaces = [] # timestep, score, and positions relavent to safety score at timestep

        # Iterate through list of length 4 tuples
        for i, pos in enumerate(craft_path):
            x, y, z, h = pos
            sides = []

            if h == 1:    # 1 = North, 8 total headings
                try:
                    sides.append((self.engine.elevation_map[y][x-1], (x-1, y)))    # 7 (Left)
                except IndexError:  # Craft has flown too close to world edge
                    penalty += 0
                try:
                    sides.append((self.engine.elevation_map[y-1][x-1], (x-1, y-1)))  # 8 (Left Diagonal)
                except IndexError:
                    penalty += 0
                try:
                    sides.append((self.engine.elevation_map[y-1][x], (x, y-1)))    # 1 (Forward)
                except IndexError:
                    penalty += 0
                try:
                    sides.append((self.engine.elevation_map[y-1][x+1], (x+1, y-1)))  # 2 (Right Diagonal)
                except IndexError:
                    penalty += 0
                try:
                    sides.append((self.engine.elevation_map[y][x+1], (x+1, y)))    # 3 (Right)
                except IndexError:
                    penalty += 0
            elif h == 2:    # 2 = Northeast
                try:
                    sides.append((self.engine.elevation_map[y-1][x-1], (x-1, y-1)))  # 8 (Left)
                except IndexError:
                    penalty += 0
                try:
                    sides.append((self.engine.elevation_map[y-1][x], (x, y-1)))    # 1 (Left Diagonal)
                except IndexError:
                    penalty += 0
                try:
                    sides.append((self.engine.elevation_map[y-1][x+1], (x+1, y-1)))  # 2 (Forward)
                except IndexError:
                    penalty += 0
                try:
                    sides.append((self.engine.elevation_map[y][x+1], (x+1, y)))    # 3 (Right Diagonal)
                except IndexError:
                    penalty += 0
                try:
                    sides.append((self.engine.elevation_map[y+1][x+1], (x+1, y+1)))  # 4 (Right)
                except IndexError:
                    penalty += 0
            elif h == 3:    # 3 = East
                try:
                    sides.append((self.engine.elevation_map[y-1][x], (x, y-1)))    # 1 (Left)
                except IndexError:
                    penalty += 0
                try:
                    sides.append((self.engine.elevation_map[y-1][x+1], (x+1, y-1)))  # 2 (Left Diagonal)
                except IndexError:
                    penalty += 0
                try:
                    sides.append((self.engine.elevation_map[y][x+1], (x+1, y)))    # 3 (Forward)
                except IndexError:
                    penalty += 0
                try:
                    sides.append((self.engine.elevation_map[y+1][x+1], (x+1, y+1)))  # 4 (Right Diagonal)
                except IndexError:
                    penalty += 0
                try:
                    sides.append((self.engine.elevation_map[y+1][x], (x, y+1)))    # 5 (Right)
                except IndexError:
                    penalty += 0
            elif h == 4:    # 4 = Southeast
                try:
                    sides.append((self.engine.elevation_map[y-1][x+1], (x+1, y-1)))  # 2 (Left)
                except IndexError:
                    penalty += 0
                try:
                    sides.append((self.engine.elevation_map[y][x+1], (x+1, y)))    # 3 (Left Diagonal)
                except IndexError:
                    penalty += 0
                try:
                    sides.append((self.engine.elevation_map[y+1][x+1], (x+1, y+1)))  # 4 (Forward)
                except IndexError:
                    penalty += 0
                try:
                    sides.append((self.engine.elevation_map[y+1][x], (x, y+1)))    # 5 (Right Diagonal)
                except IndexError:
                    penalty += 0
                try:
                    sides.append((self.engine.elevation_map[y+1][x-1], (x-1, y+1)))  # 6 (Right)
                except IndexError:
                    penalty += 0
            elif h == 5:    # 5 = South
                try:
                    sides.append((self.engine.elevation_map[y][x+1], (x+1, y)))    # 3 (Left)
                except IndexError:
                    penalty += 0
                try:
                    sides.append((self.engine.elevation_map[y+1][x+1], (x+1, y+1)))  # 4 (Left Diagonal)
                except IndexError:
                    penalty += 0
                try:
                    sides.append((self.engine.elevation_map[y+1][x], (x, y+1)))    # 5 (Forward)
                except IndexError:
                    penalty += 0
                try:
                    sides.append((self.engine.elevation_map[y+1][x-1], (x-1, y+1)))  # 6 (Right Diagonal)
                except IndexError:
                    penalty += 0
                try:
                    sides.append((self.engine.elevation_map[y][x-1], (x-1, y)))    # 7 (Right)
                except IndexError:
                    penalty += 0
            elif h == 6:    # 6 = Southwest
                try:
                    sides.append((self.engine.elevation_map[y+1][x+1], (x+1, y+1)))  # 4 (Left)
                except IndexError:
                    penalty += 0
                try:
                    sides.append((self.engine.elevation_map[y+1][x], (x, y+1)))    # 5 (Left Diagonal)
                except IndexError:
                    penalty += 0
                try:
                    sides.append((self.engine.elevation_map[y+1][x-1], (x-1, y+1)))  # 6 (Forward)
                except IndexError:
                    penalty += 0
                try:
                    sides.append((self.engine.elevation_map[y][x-1], (x-1, y)))    # 7 (Right Diagonal)
                except IndexError:
                    penalty += 0
                try:
                    sides.append((self.engine.elevation_map[y-1][x-1], (x-1, y-1)))  # 8 (Right)
                except IndexError:
                    penalty += 0
            elif h == 7:    # 7 = West
                try:
                    sides.append((self.engine.elevation_map[y+1][x], (x, y+1)))    # 5 (Left)
                except IndexError:
                    penalty += 0
                try:
                    sides.append((self.engine.elevation_map[y+1][x-1], (x-1, y+1)))  # 6 (Left Diagonal)
                except IndexError:
                    penalty += 0
                try:
                    sides.append((self.engine.elevation_map[y][x-1], (x-1, y)))    # 7 (Forward)
                except IndexError:
                    penalty += 0
                try:
                    sides.append((self.engine.elevation_map[y-1][x-1], (x-1, y-1)))  # 8 (Right Diagonal)
                except IndexError:
                    penalty += 0
                try:
                    sides.append((self.engine.elevation_map[y-1][x], (x, y-1)))    # 1 (Right)
                except IndexError:
                    penalty += 0
            elif h == 8:    # 8 = Northwest
                try:
                    sides.append((self.engine.elevation_map[y+1][x-1], (x-1, y+1)))  # 6 (Left)
                except IndexError:
                    penalty += 0
                try:
                    sides.append((self.engine.elevation_map[y][x-1], (x-1, y)))    # 7 (Left Diagonal)
                except IndexError:
                    penalty += 0
                try:
                    sides.append((self.engine.elevation_map[y-1][x-1], (x-1, y-1)))  # 8 (Forward)
                except IndexError:
                    penalty += 0
                try:
                    sides.append((self.engine.elevation_map[y-1][x], (x, y-1)))    # 1 (Right Diagonal)
                except IndexError:
                    penalty += 0
                try:
                    sides.append((self.engine.elevation_map[y-1][x+1], (x+1, y-1)))  # 2 (Right)
                except IndexError:
                    penalty += 0

            penalty_pos = []    # List for keeping penalty positions for this timestep

            # Check each side to see if it flew close to dangerous obstacles
            for side, pos in sides:
                # If elevation at side is greater than or equal to altitude at this position, increase penalty
                if side >= z:
                    penalty_pos.append((2.0, pos))
                    penalty += 2

            # If a crash has been recorded, then increase penalty (dont include exiting AOI)
            if self.engine.inside_AOI(x, y) and self.engine.elevation_map[y][x] >= z:
                penalty += 200

            if total != penalty:    # Use checksum - only if penalty has changed add an entry
                saftey_relavent_spaces.append((i+offset, penalty, penalty_pos)) # add timestep + relavent positions
                total = penalty
            # print("Score at {} is {}".format(pos, penalty))

        # print("Final is {}".format(penalty))

        return penalty, saftey_relavent_spaces

    # Per segment in path: (1.5*angular moves) + (1 * straight moves) + (0.5*number of alt changes)
    def score_craft_distance(self, path, offset):

        penalty = 0.0
        try:
            current_heading = path[0][3]
            current_alt = path[0][2]
        except IndexError:
            return penalty, [(0, 0.0, []), (offset, 0.0, [])]

        distance_relavent_spaces = []   # timestep, score, and relavent positions

        angular = 0
        straight = 0
        alt_changes = 0
        straight_moves = [1, 3, 5, 7]

        for i, pos in enumerate(path[1:]):    # Already have first heading, go through rest of the path
            alt_changed = False
            straight_move = True

            if pos[3] in straight_moves:   # Check if craft made a straight move
                straight += 1
            else:   # Otherwise, craft heading is different and craft has made an angular move
                angular += 1
                straight_move = False

            if current_alt != pos[2]:    # If altitude is not equivalent, then there has been a change
                alt_changes += 1
                alt_changed = True

            # Update values
            current_alt = pos[2]

            penalty = straight + 1.5*angular + 0.5*alt_changes
            local = 1.0 if straight_move else 1.5   # Compute local position penalty
            distance_relavent_spaces.append((i+offset, penalty, (local if not alt_changed else local+0.5, (pos[0], pos[1]))))    # Offset 

        return penalty, distance_relavent_spaces

    # Calculate path of hiker to package using A*.  2*(path length) + 10*(river spaces in path) + 2*(number of changes in elevation in path) + los penalty
    def score_hiker_difficulty(self, hiker_pos, drop_pos, drop_time):

        hiker_river_crossing = 0.0
        hiker_climbing = 0.0
        hiker_route_length = 0.0

        route_length_multiplier = 8 # Multiplier for importance of hiker route length

        hiker_river_positions = [] # Positions that affect hiker river crossing score
        hiker_climbing_positions = [] # Positions that affect hiker climbing score
        hiker_route_positions = []  # Positions that affect hiker route length score

        def heuristic(cx, cy, nx, ny, tile):
            dist = euclidean_distance(cx, cy, nx, ny)
            tile_penalty = 0.0
            if tile == 15:
                tile_penalty = 5
            elif tile == 24:
                tile_penalty = 2

            return dist + tile_penalty

        # print("Hiker is travelling from: {} to {}".format(hiker_pos, drop_pos))
        h_straight = 0
        h_angled = 0
        rivers_in_path = 0
        changes_in_elevation = 0
        hills_in_path = 0

        # Use A* search to get path
        came_from = {}
        cost_so_far = {}
        frontier = []
        current = tuple(hiker_pos)

        # Init starting pos
        found = False
        came_from[current] = [current]
        cost_so_far[current] = 0
        heapq.heappush(frontier, (0, current))

        # Hiker has 8 degrees of movement
        moves = [(0, -1, 1), (1, -1, 2), (1, 0, 3), (1, 1, 4), (0, 1, 5), (-1, 1, 6), (-1, 0, 7), (-1, -1, 8)]
        straight_moves = [1, 3, 5, 7]
        open_tiles = [1, 2, 10, 15] # Tiles at elevation 0 - small hill (24) is also considered in A*

        # Solve A*
        while len(frontier) != 0:
            current = heapq.heappop(frontier)[1]
            # print(current)
            x, y, z, h = current

            if current[:3] == drop_pos[:3]:
                found = True
                break

            neighbors = []
            for move in moves:
                nx, ny, nh = move
                nx += x
                ny += y
                if nx >= 0 and nx < self.engine.AOI[2] and ny >= 0 and ny < self.engine.AOI[3]:
                    tile = self.engine.tile_map[ny][nx]
                    nz = self.engine.elevation_map[ny][nx]
                    if tile in open_tiles or tile == 24:
                        n = (nx, ny, nz, nh)
                        h = heuristic(x, y, n[0], n[1], tile)
                        move_cost = 1 if n[3] in straight_moves else 1.5
                        next_cost = move_cost + cost_so_far[current]
                
                        if n not in cost_so_far or next_cost < cost_so_far[n]:
                            next_path = copy.copy(came_from[current])
                            next_path.append(n)
                            came_from[n] = next_path
                            cost_so_far[n] = next_cost
                            heapq.heappush(frontier, (h + next_cost, n))

        # If the hiker is able to reach the package
        if found:
            final_path = []

            for heading in range(1, 9): # Try all headings
                end_position = (drop_pos[0], drop_pos[1], drop_pos[2], heading)
                if end_position not in came_from:
                    continue

                a_path = came_from[end_position]
                a_path_cost = cost_so_far[a_path[-1]]
                final_path_cost = cost_so_far[final_path[-1]] if len(final_path) != 0 else float('inf')

                # Take the least costing path
                if len(final_path) == 0 or a_path_cost < final_path_cost:
                    final_path = a_path

            old_z = hiker_pos[2]    # Hiker altitude
            h_straight = 0
            h_angled = 0

            angle_route_score = 12 # 1.5 * 8

            # Iterate through path to calculate penalty
            for pos in final_path:
                x, y, z, h = pos
                tile = self.engine.tile_map[y][x]

                # Check if hiker move is angled or straight
                if h in straight_moves:
                    h_straight += 1
                    hiker_route_positions.append((route_length_multiplier, (pos[0], pos[1])))   # Append positions to relavent hiker path
                else:
                    h_angled += 1
                    hiker_route_positions.append((angle_route_score, (pos[0], pos[1])))   # Append positions to relavent hiker path
                # Check if this space is a river
                if tile == 15.0:
                    rivers_in_path += 1
                    hiker_river_positions.append((10, (pos[0], pos[1])))
                elif tile == 24:
                    hills_in_path += 1
                    hiker_climbing_positions.append((5, (pos[0], pos[1])))
                # Check if there is an elevation change
                if z != old_z:
                    changes_in_elevation += 1
                    old_z = z
                    hiker_climbing_positions.append((2, (pos[0], pos[1])))
                # print("Score at {} on tile {} is 4*({} + 1.5*{}) = {}".format(pos, tile, h_straight, h_angled, 4*(h_straight+1.5*h_angled)))
                
            hiker_route_length = route_length_multiplier*(h_straight + 1.5*h_angled)
        else:
            print("Hiker unable to reach the package!")
            hiker_route_length = 200.0

        hiker_river_crossing = 4*10*rivers_in_path
        hiker_climbing = 4*2*changes_in_elevation + 4*5*hills_in_path

        #  Add an empty entry because python JSON collapses seemingly redundant lists/tuples
        hiker_river_relavent_spaces = [(0, 0.0, []), (drop_time, hiker_river_crossing, hiker_river_positions)] # time, score, positions that affect hiker river crossing score
        hiker_climbing_relavent_spaces = [(0, 0.0, []), (drop_time, hiker_climbing, hiker_climbing_positions)] # time, score, and positions that affect hiker climbing score
        hiker_route_relavent_spaces = [(0, 0.0, []), (drop_time, hiker_route_length, hiker_route_positions)]  # time, score, and ositions that affect hiker route length score

        return hiker_river_crossing, hiker_climbing, hiker_route_length, hiker_river_relavent_spaces, hiker_climbing_relavent_spaces, hiker_route_relavent_spaces

    def score_package_los(self, hiker_pos, drop_pos, drop_time):
        # Plot a straight line from hiker pos to drop pos (integer raytracing equation based on Bresenham line algorithm)
        los_path = []
        low_obs_in_path = 0
        med_obs_in_path = 0
        hi_obs_in_path = 0

        if drop_pos[:2] == hiker_pos:   # Drone dropped on hiker
            return 0.0, [(0, 0.0, []), (drop_time, 0.0, [(0.0, (drop_pos[0], drop_pos[1]))])]

        delta_x = int(abs(drop_pos[0] - hiker_pos[0]))
        delta_y = int(abs(drop_pos[1] - hiker_pos[1]))
        n = 1 + delta_x + delta_y   # How far along the line are we?

        # Determine direction of increment
        x_increment = 1 if (drop_pos[0] > hiker_pos[0]) else -1
        y_increment = 1 if (drop_pos[1] > hiker_pos[1]) else -1
        error = delta_x - delta_y
        
        delta_x *= 2
        delta_y *= 2
        
        line_x, line_y, line_z, line_h = hiker_pos
        for _ in range(n, 0, -1):  # Decrement until done (reach 0)
            los_path.append((int(line_x), int(line_y)))
            if error > 0:
                line_x += x_increment
                error -= delta_y
            else:
                line_y += y_increment
                error += delta_x

        elevation_obstacles_low = [1, 13, 17]   # Trees and towers
        elevation_obstacles_med = 24            # Hills at elevation 1
        elevation_obstacles_hi = [25, 26, 31]   # Mountains at elevation 2+

        los_positions = []  # Positions relavent to los

        # Process LOS path
        for lpos in los_path:
            if self.engine.inside_AOI(lpos[0], lpos[1]):
                tile = self.engine.tile_map[lpos[1]][lpos[0]]

                los_positions.append((0, lpos)) # help visualize line of sight path

                if tile in elevation_obstacles_low:
                    low_obs_in_path += 1
                    los_positions.append((1, lpos))
                if tile == elevation_obstacles_med:
                    med_obs_in_path += 1
                    los_positions.append((2, lpos))
                if tile in elevation_obstacles_hi:
                    hi_obs_in_path += 1
                    los_positions.append((50, lpos))

        penalty = 4*(low_obs_in_path + 2*med_obs_in_path + 50*hi_obs_in_path)
        #  Add an empty entry because python JSON collapses seemingly redundant lists/tuples
        los_relavent_spaces = [(0, 0.0, []), (drop_time, penalty, los_positions)]  # Single timestep, multiple positions

        return penalty, los_relavent_spaces

    # 50 * (obs) / # of tiles in drop zone
    def score_package_safety(self, drop_point, drop_time):
        dmg_penalty = 0.0
        inaccess_penalty = 0.0

        if drop_point is None:
            return dmg_penalty, (inaccess_penalty+100.0), [(0, 0.0, []), (drop_time, 0.0, [(0.0, ())])], [(0, 0.0, []), (drop_time, inaccess_penalty+100.0, [(0.0, ())])]

        drop_x, drop_y, drop_z, drop_h = drop_point

        dmg_obstacles = [15, 13, 17]
        inaccess_obstacles = [1, 25, 26, 31]

        inaccess_positions = []
        dmg_positions = [] 

        if drop_z == 1:
            if self.engine.inside_AOI(drop_x, drop_y):
                tile = self.engine.tile_map[drop_y][drop_x]
                if tile in inaccess_obstacles:
                    inaccess_penalty = 90.0
                    inaccess_positions.append((90, (drop_x, drop_y)))
                else:
                    inaccess_positions.append((0, (drop_x, drop_y)))    # Append values of 0 for showing drop area
                if tile in dmg_obstacles or self.engine.elevation_map[drop_y][drop_x] >= 1:
                    dmg_penalty = 100.0
                    dmg_positions.append((100, (drop_x, drop_y)))
                else:
                    dmg_positions.append((0, (drop_x, drop_y)))
        elif drop_z == 2:
            for dx in range(drop_x-1, drop_x+2):
                for dy in range(drop_y-1, drop_y+2):
                    if self.engine.inside_AOI(dx, dy):
                        inaccess_positions.append((0, (dx, dy)))    # Append values of 0 for showing drop area
                        dmg_positions.append((0, (dx, dy)))
                        tile = self.engine.tile_map[dy][dx]
                        if tile in inaccess_obstacles:
                            inaccess_penalty = inaccess_penalty + 90.0
                            inaccess_positions.append((90, (dx, dy)))
                        if tile in dmg_obstacles or self.engine.elevation_map[dy][dx] >= 2:
                            # Damaged if package impacts side of obstacle higher than craft due to package scatter
                            dmg_penalty = dmg_penalty + 100.0
                            dmg_positions.append((100, (dx, dy)))
            dmg_penalty = dmg_penalty / 9
            inaccess_penalty = inaccess_penalty / 9   # Get average across scatter zone
        elif drop_z == 3:
            for dx in range(drop_x-2, drop_x+3):
                for dy in range(drop_y-2, drop_y+3):
                    if self.engine.inside_AOI(dx, dy):
                        tile = self.engine.tile_map[dy][dx]
                        elevation = self.engine.elevation_map[dy][dx]
                        inaccess_positions.append((0, (dx, dy)))    # Append values of 0 for showing drop area
                        dmg_positions.append((0, (dx, dy)))
                        if tile in inaccess_obstacles:
                            inaccess_penalty = inaccess_penalty + 90.0
                            inaccess_positions.append((90, (dx, dy)))
                        if tile in dmg_obstacles or elevation >= 3:
                            # Damaged if package impacts side of obstacle higher than craft due to package scatter
                            dmg_penalty = dmg_penalty + 100.0
                            dmg_positions.append((100, (dx, dy)))
            dmg_penalty = dmg_penalty / 25
            inaccess_penalty = inaccess_penalty / 25 # Get average across scatter zone

        #  Add an empty entry because python JSON collapses seemingly redundant lists/tuples
        return dmg_penalty, inaccess_penalty, [(0, 0, []), (drop_time, dmg_penalty, dmg_positions)], [(0, 0, []), (drop_time, inaccess_penalty, inaccess_positions)]